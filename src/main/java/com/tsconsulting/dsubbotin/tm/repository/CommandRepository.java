package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.constant.ArgumentConst;
import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;
import com.tsconsulting.dsubbotin.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT,
            ArgumentConst.ABOUT,
            "Display developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION,
            ArgumentConst.VERSION,
            "Display program version."
    );

    private static final Command INFO = new Command(
            TerminalConst.INFO,
            ArgumentConst.INFO,
            "Display system info."
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP,
            ArgumentConst.HELP,
            "Display list of terminal commands."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS,
            ArgumentConst.COMMANDS,
            "Display list commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS,
            ArgumentConst.ARGUMENTS,
            "Display list arguments."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Display task list."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Remove all tasks."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null, "Display task by id."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null, "Display task by index."
    );

    private static final Command TASK_SHOW_BY_NAME = new Command(
            TerminalConst.TASK_SHOW_BY_NAME, null, "Display task by name."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove task by id."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update task by id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null, "Start task by id."
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null, "Start task by index."
    );

    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.TASK_START_BY_NAME, null, "Start task by name."
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.TASK_FINISH_BY_ID, null, "Finish task by id."
    );

    private static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.TASK_FINISH_BY_INDEX, null, "Finish task by index."
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.TASK_FINISH_BY_NAME, null, "Finish task by name."
    );

    private static final Command TASK_UPDATE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_STATUS_BY_ID, null, "Update status task by id."
    );

    private static final Command TASK_UPDATE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_STATUS_BY_INDEX, null, "Update status task by index."
    );

    private static final Command TASK_UPDATE_STATUS_BY_NAME = new Command(
            TerminalConst.TASK_UPDATE_STATUS_BY_NAME, null, "Update status task by name."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Display project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Remove all projects."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null, "Display project by id."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null, "Display project by index."
    );

    private static final Command PROJECT_SHOW_BY_NAME = new Command(
            TerminalConst.PROJECT_SHOW_BY_NAME, null, "Display project by name."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null, "Start project by id."
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null, "Start project by index."
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.PROJECT_START_BY_NAME, null, "Start project by name."
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.PROJECT_FINISH_BY_ID, null, "Finish project by id."
    );

    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.PROJECT_FINISH_BY_INDEX, null, "Finish project by index."
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.PROJECT_FINISH_BY_NAME, null, "Finish project by name."
    );

    private static final Command PROJECT_UPDATE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_STATUS_BY_ID, null, "Update status project by id."
    );

    private static final Command PROJECT_UPDATE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_STATUS_BY_INDEX, null, "Update status project by index."
    );

    private static final Command PROJECT_UPDATE_STATUS_BY_NAME = new Command(
            TerminalConst.PROJECT_UPDATE_STATUS_BY_NAME, null, "Update status project by name."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Exit application."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, VERSION, INFO, HELP, COMMANDS, ARGUMENTS,
            TASK_LIST, TASK_CLEAR, TASK_CREATE,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_SHOW_BY_NAME,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_FINISH_BY_ID, TASK_FINISH_BY_INDEX, TASK_FINISH_BY_NAME,
            TASK_UPDATE_STATUS_BY_ID, TASK_UPDATE_STATUS_BY_INDEX, TASK_UPDATE_STATUS_BY_NAME,
            PROJECT_LIST, PROJECT_CLEAR, PROJECT_CREATE,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME,
            PROJECT_FINISH_BY_ID, PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_NAME,
            PROJECT_UPDATE_STATUS_BY_ID, PROJECT_UPDATE_STATUS_BY_INDEX, PROJECT_UPDATE_STATUS_BY_NAME,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
