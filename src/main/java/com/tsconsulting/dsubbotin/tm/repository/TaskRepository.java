package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findById(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        if (tasks.size() - 1 < index) return null;
        return tasks.get(index);
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task updateBuyId(final String id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateBuyIndex(final int index, final String name, final String description) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task updateStatusById(final String id, final Status status) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateStatusByIndex(final int index, final Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task updateStatusByName(final String name, final Status status) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}
