package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(final String name);

    void create(final String name, final String description);

    void add(final Project project);

    void remove(final Project project);

    List<Project> findAll();

    void clear();

    Project findById(final String id);

    Project findByIndex(final int index);

    Project findByName(final String name);

    Project removeById(final String id);

    Project removeByIndex(final int index);

    Project removeByName(final String name);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final int index, final String name, final String description);

    Project startById(final String id);

    Project startByIndex(final int index);

    Project startByName(final String name);

    Project finishById(final String id);

    Project finishByIndex(final int index);

    Project finishByName(final String name);

    Project updateStatusById(final String id, final Status status);

    Project updateStatusByIndex(final int index, final Status status);

    Project updateStatusByName(final String name, final Status status);

}
